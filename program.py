import numpy as np
import scipy.optimize as opt
import matplotlib.pyplot as plt

def f(x):
    y = 3*x**2+4*x-1-4*np.log10(x+3)
    return y

#Roots
roots=[]
for a in np.linspace(-2,2, 9):
    roots.append(round(opt.fsolve(f,a)[0],4))
roots=list(set(roots))
print(roots)

#Some plots
#First expresion
x=np.linspace(-2,2)
y1 = 3*x**2+4*x-1
plt.plot(x,y1)
#Second expression
y2= 4*np.log10(x+3)
plt.plot(x,y2)

plt.plot(roots[0], 4*np.log10(roots[0]+3), 'ro')
plt.plot(roots[1], 4*np.log10(roots[1]+3),'ro')

plt.show()



