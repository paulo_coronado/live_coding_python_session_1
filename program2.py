import numpy as np
import scipy.optimize as opt
import matplotlib.pyplot as plt

#Distance vs Time plot
t=np.linspace(0,5)
distance= t*np.sin(t**2)+5
#distance=  np.exp(-t**2)
plt.plot(t,distance)

#Derivative plot
# d(x) = (f (x + h) − f (x − h))/2h
# When h tends to 0

h=0.001

d= (((t+h)*np.sin((t+h)**2)+5) - ((t-h)*np.sin((t-h)**2)+5))/(2*h)
#d= (np.exp(-(t+h)**2)-np.exp(-(t-h)**2))/(2*h)
print(d)
plt.plot(t,d, '--')

plt.plot(t,t-t)
plt.show()
